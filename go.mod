module gitlab.com/xusz/rome-todos

go 1.13

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.0
	github.com/vektah/gqlparser/v2 v2.0.1
	gitlab.com/momentum-valley/adam v0.2.78-beta
	gitlab.com/xusz/todo v0.0.3-xusz
	google.golang.org/protobuf v1.24.0
)

// replace gitlab.com/xusz/todo => /home/user/projects/todo
