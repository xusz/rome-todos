package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"gitlab.com/xusz/rome-todos/internal/generated"
	"gitlab.com/xusz/rome-todos/internal/resolvers"
	"gitlab.com/xusz/rome-todos/internal/utils"
)

var configFilename string
var configDirs string

func init() {
	const (
		defaultConfigFilename = "config"
		configUsage           = "Name of the config file, without extension"
		defaultConfigDirs     = "./,./configs/"
		configDirUsage        = "Directories to search for config file, separated by ','"
	)
	flag.StringVar(&configFilename, "c", defaultConfigFilename, configUsage)
	flag.StringVar(&configFilename, "config", defaultConfigFilename, configUsage)
	flag.StringVar(&configDirs, "cPath", defaultConfigDirs, configDirUsage)
}

func main() {
	flag.Parse()

	// Setting up configurations
	err := utils.InitConfiguration(configFilename, strings.Split(configDirs, ","))
	if err != nil {
		panic(fmt.Errorf("Error parsing config, %s", err))
	}

	utils.InitLogger()

	if utils.CONFIG.DebugMode {
		utils.Logger.Warn("running on debug mode")
	}

	// Populating RPC Clients
	rpcClients := utils.RPCClients{}
	utils.PopulateRPCClientsWithConfig(&rpcClients, &utils.CONFIG)

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &resolvers.Resolver{RPCClients: &rpcClients}}))
	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", utils.CONFIG.Service.Port)
	log.Fatal(http.ListenAndServe(":"+utils.CONFIG.Service.Port, nil))
}
