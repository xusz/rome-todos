package resolvers

import (
	"context"

	"gitlab.com/xusz/rome-todos/internal/generated"
	"gitlab.com/xusz/rome-todos/internal/utils"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	RPCClients *utils.RPCClients
}

// Mutation returns MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }

func (r *mutationResolver) Healthcheck(ctx context.Context) (string, error) {
	return "ack", nil
}

func (r *queryResolver) Healthcheck(ctx context.Context) (string, error) {
	return "ack", nil
}
