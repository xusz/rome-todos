package resolvers

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/xusz/rome-todos/internal/utils"
	todo "gitlab.com/xusz/todo/rpc/todo"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

func (r *queryResolver) Todos(ctx context.Context, first *int, after *string, last *int, before *string, sortBy todo.TodoSortType, isDesc bool, userID string) (*todo.TodoConnection, error) {
	utils.Logger.Traceln("Todos, userID: ", userID)

	req := todo.GetTodosRequest{
		SortBy: sortBy,
		IsDesc: isDesc,
		UserId: userID,
	}
	if first != nil {
		req.First = &wrapperspb.Int32Value{Value: int32(*first)}
	}

	response, err := (*r.RPCClients.Todo).GetTodos(ctx, &req)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *queryResolver) Todo(ctx context.Context, id string, userID string) (*todo.Todo, error) {
	utils.Logger.Traceln("Todo, userID: ", userID)

	req := todo.GetTodoRequest{
		Id:     id,
		UserId: userID,
	}

	response, err := (*r.RPCClients.Todo).GetTodo(ctx, &req)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *mutationResolver) CreateTodo(ctx context.Context, input todo.CreateTodoRequest) (*todo.Todo, error) {
	utils.Logger.Traceln("CreateTodo, input: ", utils.LogJSONData(input))

	response, err := (*r.RPCClients.Todo).CreateTodo(ctx, &input)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *mutationResolver) UpdateTodo(ctx context.Context, input todo.UpdateTodoRequest) (*todo.Todo, error) {
	utils.Logger.Traceln("UpdateTodo, input: ", utils.LogJSONData(input))

	response, err := (*r.RPCClients.Todo).UpdateTodo(ctx, &input)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *mutationResolver) DeleteTodo(ctx context.Context, input todo.DeleteTodoRequest) (*wrapperspb.BoolValue, error) {
	utils.Logger.Traceln("DeleteTodo, input: ", utils.LogJSONData(input))

	response, err := (*r.RPCClients.Todo).DeleteTodo(ctx, &input)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *mutationResolver) SetTodoDone(ctx context.Context, userID string, id string) (*wrapperspb.BoolValue, error) {
	utils.Logger.Tracef("SetTodoDone, userID[%s], todoID[%s]: ", userID, id)

	input := todo.SetTodoDoneRequest{
		Id:     id,
		UserId: userID,
	}
	response, err := (*r.RPCClients.Todo).SetTodoDone(ctx, &input)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *mutationResolver) CreateUser(ctx context.Context, input todo.CreateUserRequest) (*todo.User, error) {
	utils.Logger.Traceln("CreateUser, input: ", utils.LogJSONData(input))

	response, err := (*r.RPCClients.Todo).CreateUser(ctx, &input)
	if err != nil {
		return nil, err
	}

	return response, nil
}
