package utils

import (
	"net/http"

	todo "gitlab.com/xusz/todo/rpc/todo"
)

// RPCClients ...
type RPCClients struct {
	Todo *todo.TodoService
}

// PopulateRPCClientsWithConfig ...
func PopulateRPCClientsWithConfig(clients *RPCClients, configs *Configuration) error {
	if err := populateTodoClient(clients, &configs.RPC.Todo); err != nil {
		return err
	}

	return nil
}

func populateTodoClient(clients *RPCClients, configs *TodoConfiguration) error {
	var client = todo.NewTodoServiceProtobufClient(configs.URI, &http.Client{})
	clients.Todo = &client
	return nil
}
