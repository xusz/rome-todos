package utils

import (
	"github.com/spf13/viper"
)

// CONFIG  global configuration
var CONFIG Configuration

// Configuration ...
type Configuration struct {
	Service   ServiceConfiguration
	RPC       RPCConfiguration
	DebugMode bool
}

// ServiceConfiguration ...
type ServiceConfiguration struct {
	Port string
}

// RPCConfiguration ...
type RPCConfiguration struct {
	Todo TodoConfiguration
}

// TodoConfiguration ...
type TodoConfiguration struct {
	URI string
}

// InitConfiguration ...
func InitConfiguration(configName string, configPaths []string) error {
	vp := viper.New()
	vp.SetConfigName(configName)
	vp.AutomaticEnv()
	for _, configPath := range configPaths {
		vp.AddConfigPath(configPath)
	}

	if err := vp.ReadInConfig(); err != nil {
		return err
	}

	err := vp.Unmarshal(&CONFIG)
	if err != nil {
		return err
	}

	return nil
}
