package utils

import (
	"encoding/json"
	"log"
	"runtime/debug"
)

// RecoverPanic ...
func RecoverPanic(isPrintStack bool) interface{} {
	if r := recover(); r != nil {
		Logger.Error(r)
		if isPrintStack {
			Logger.Error(debug.Stack())
		}

		return r
	}

	return nil
}

// LogJSONData ...
func LogJSONData(data interface{}) string {
	if !CONFIG.DebugMode {
		return ""
	}
	raw, err := json.Marshal(data)
	if err != nil {
		log.Println(err)
		return ""
	}
	return string(raw)
}
