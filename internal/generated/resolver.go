package generated

// THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

import (
	"context"

	gqlgen_todos "gitlab.com/xusz/todo/rpc/todo"
	"google.golang.org/protobuf/types/known/wrapperspb"
)

type Resolver struct{}

func (r *mutationResolver) Healthcheck(ctx context.Context) (string, error) {
	panic("not implemented")
}

func (r *mutationResolver) CreateUser(ctx context.Context, input gqlgen_todos.CreateUserRequest) (*gqlgen_todos.User, error) {
	panic("not implemented")
}

func (r *mutationResolver) CreateTodo(ctx context.Context, input gqlgen_todos.CreateTodoRequest) (*gqlgen_todos.Todo, error) {
	panic("not implemented")
}

func (r *mutationResolver) UpdateTodo(ctx context.Context, input gqlgen_todos.UpdateTodoRequest) (*gqlgen_todos.Todo, error) {
	panic("not implemented")
}

func (r *mutationResolver) DeleteTodo(ctx context.Context, input gqlgen_todos.DeleteTodoRequest) (*wrapperspb.BoolValue, error) {
	panic("not implemented")
}

func (r *mutationResolver) SetTodoDone(ctx context.Context, userID string, id string) (*wrapperspb.BoolValue, error) {
	panic("not implemented")
}

func (r *queryResolver) Healthcheck(ctx context.Context) (string, error) {
	panic("not implemented")
}

func (r *queryResolver) Todos(ctx context.Context, first *int, after *string, last *int, before *string, sortBy gqlgen_todos.TodoSortType, isDesc bool, userID string) (*gqlgen_todos.TodoConnection, error) {
	panic("not implemented")
}

func (r *queryResolver) Todo(ctx context.Context, id string, userID string) (*gqlgen_todos.Todo, error) {
	panic("not implemented")
}

// Mutation returns MutationResolver implementation.
func (r *Resolver) Mutation() MutationResolver { return &mutationResolver{r} }

// Query returns QueryResolver implementation.
func (r *Resolver) Query() QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
