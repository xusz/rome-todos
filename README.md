# rome-todos

Rome is the single GraphQL endpoint dealing with frontend requests. It checks user authorization with a standard RBAC scheme and invokes specific business services through RPC.
